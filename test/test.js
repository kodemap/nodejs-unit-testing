/* global exports */

(function () {
  'use strict';

  var app = require('../app');

  // multiply test unit
  exports.multiply = {
    "should multiply 2 by 3": function (assert) {
      assert.expect(1);
      assert.equal(6, app.multiply(2, 3));
      assert.done();
    }
  };

  // divide test unit
  exports.divide = {
    "should divide 4 by 2": function (assert) {
      assert.expect(1);
      assert.equal(2, app.divide(4, 2));
      assert.done();
    }
  };
})();
