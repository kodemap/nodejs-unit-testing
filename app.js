/* global exports */

(function () {
  'use strict';

  /**
   * Returns the product of the <i>multiplicand</i> and the <i>multiplier</i>.
   *
   * @params multiplicand
   * @params multiplier
   * @returns {number}
   */
  exports.multiply = function (multiplicand, multiplier) {
    return multiplicand * multiplier;
  };

  /**
   * Returns the quotient of the <i>dividend</i> by the <i>divider</i>.
   *
   * @params dividend
   * @params divider
   * @returns {number}
   */
  exports.divide = function (dividend, divider) {
    return dividend / divider;
  };
})();
